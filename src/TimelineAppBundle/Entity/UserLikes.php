<?php

namespace TimelineAppBundle\Entity;

class UserLikes
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Timeline
     */
    private $timeline;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        $user->addLike($this);
    }

    /**
     * @return Timeline
     */
    public function getTimeline()
    {
        return $this->timeline;
    }

    /**
     * @param Timeline $timeline
     */
    public function setTimeline(Timeline $timeline)
    {
        $this->timeline = $timeline;
        $timeline->addLike($this);
    }
}
