<?php

namespace TimelineAppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use TimelineAppBundle\Entity\Timeline;

class TimelineRepository extends EntityRepository
{
    /**
     * @param string $projectName
     * @return Timeline[]
     */
    public function search($projectName)
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('t,u,ul')
            ->from('TimelineAppBundle:Timeline', 't')
            ->innerJoin('t.user', 'u')
            ->leftJoin('u.likes', 'ul')
            ->andWhere('t.name LIKE :name')
            ->setParameter('name', '%' . $projectName . '%')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $userSlug
     * @param string $timelineSlug
     * @return Timeline
     */
    public function findBySlugs($userSlug, $timelineSlug)
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('t')
            ->from('TimelineAppBundle:Timeline', 't')
            ->innerJoin('t.user', 'u')
            ->andWhere('u.slug = :userSlug')
            ->andWhere('t.slug = :timelineSlug')
            ->setParameter('userSlug', $userSlug)
            ->setParameter('timelineSlug', $timelineSlug)
            ->getQuery()
            ->getSingleResult();
    }
}
