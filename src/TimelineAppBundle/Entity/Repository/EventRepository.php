<?php

namespace TimelineAppBundle\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use TimelineAppBundle\Entity\Event;
use TimelineAppBundle\Entity\Timeline;
use TimelineAppBundle\Timeline\Filters\EventFilters;

class EventRepository extends EntityRepository
{
    /**
     * @param Timeline $timeline
     * @return Event[]
     */
    public function findByTimeline(Timeline $timeline)
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('TimelineAppBundle:Event', 'e')
            ->innerJoin('e.timeline', 't')
            ->andWhere('t.id = :timelineId')
            ->setParameter('timelineId', $timeline->getId())
            ->orderBy('e.date', 'asc')
            ->getQuery()
            ->getResult();
    }

    public function findEvents(Timeline $timeline, EventFilters $filters, $limit)
    {
        $qb = $this->getBaseEventFilters($timeline, $filters);

        return $qb
            ->select('e,tg')
            ->innerJoin('e.tags', 'tg')
            ->orderBy('e.date', 'asc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param Timeline $timeline
     * @param EventFilters $filters
     * @return int
     */
    public function countEvents(Timeline $timeline, EventFilters $filters)
    {
        $qb = $this->getBaseEventFilters($timeline, $filters);

        return (int) $qb
            ->select('count(e.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Timeline $timeline
     * @return Event
     */
    public function findFirstEvent(Timeline $timeline)
    {
        return $this->findSpecificEvent($timeline, 'asc');
    }

    /**
     * @param Timeline $timeline
     * @return Event
     */
    public function findLastEvent(Timeline $timeline)
    {
        return $this->findSpecificEvent($timeline, 'desc');
    }

    /**
     * @param Timeline $timeline
     * @param $sortDirection
     * @return Event
     */
    private function findSpecificEvent(Timeline $timeline, $sortDirection)
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('e')
            ->from('TimelineAppBundle:Event', 'e')
            ->innerJoin('e.timeline', 't')
            ->andWhere('t.id = :timelineId')
            ->setParameter('timelineId', $timeline->getId())
            ->setMaxResults(1)
            ->orderBy('e.date', $sortDirection)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @param Timeline $timeline
     * @param EventFilters $filters
     * @return QueryBuilder
     */
    private function getBaseEventFilters(Timeline $timeline, EventFilters $filters)
    {
        $qb = $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->from('TimelineAppBundle:Event', 'e')
            ->innerJoin('e.timeline', 't')
            ->andWhere('t.id = :timelineId')
            ->setParameter('timelineId', $timeline->getId());

        if ($filters->hasDateFrom()) {
            $qb
                ->andWhere('e.date >= :startDate')
                ->setParameter('startDate', $filters->getDateFrom());
        }

        if ($filters->hasDateTo()) {
            $qb
                ->andWhere('e.date <= :endDate')
                ->setParameter('endDate', $filters->getDateTo());
        }

        return $qb;
    }
}
