<?php

namespace TimelineAppBundle\Entity;

use Cocur\Slugify\Slugify;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;

class User implements UserInterface, \Serializable
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $slug;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $email;

    /**
     * @var Timeline[]
     */
    private $timelines;

    /**
     * @var UserLikes[]
     */
    private $likes;

    public function __construct()
    {
        $this->likes = new ArrayCollection();
        $this->timelines = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
        $slug = new Slugify();
        $this->slug = $slug->slugify($username);
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @return null
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials()
    {
    }

    /**
     * @return string
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->password
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->password
        ) = unserialize($serialized);
    }

    /**
     * @return UserLikes[]
     */
    public function getLikes()
    {
        return $this->likes;
    }

    /**
     * @param array $likes
     */
    public function setLikes(array $likes)
    {
        $this->likes = $likes;
    }

    public function addLike(UserLikes $like)
    {
        $this->likes->add($like);
    }

    public function removeLike(UserLikes $like)
    {
        $this->likes->removeElement($like);
    }

    /**
     * @return Timeline[]
     */
    public function getTimelines()
    {
        return $this->timelines;
    }

    /**
     * @param array $timelines
     */
    public function setTimelines(array $timelines)
    {
        $this->timelines = $timelines;
    }

    public function addTimeline(Timeline $timeline)
    {
        $this->timelines->add($timeline);
    }

    public function removeTimeline(Timeline $timeline)
    {
        $this->timelines->removeElement($timeline);
    }
}
