<?php

namespace TimelineAppBundle\Timeline\Filters;

use Symfony\Component\HttpFoundation\RequestStack;

class EventFilters
{
    /**
     * @var \DateTime
     */
    private $dateFrom;

    /**
     * @var \DateTime
     */
    private $dateTo;

    /**
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->request = $requestStack->getCurrentRequest();
    }

    public function create()
    {
        if ($this->request->query->has('from')) {
            $dateFrom = new \DateTime();
            $dateFrom->setTimestamp($this->request->query->get('from'));
            $this->dateFrom = $dateFrom;
        }

        if ($this->request->query->has('to')) {
            $dateTo = new \DateTime();
            $dateTo->setTimestamp($this->request->query->get('to'));
            $this->dateTo = $dateTo;
        }
    }

    /**
     * @return \DateTime
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * @return boolean
     */
    public function hasDateTo()
    {
        return $this->dateTo !== null;
    }

    /**
     * @param \DateTime $dateTo
     */
    public function setDateTo($dateTo)
    {
        $this->dateTo = $dateTo;
    }

    /**
     * @return \DateTime
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * @return boolean
     */
    public function hasDateFrom()
    {
        return $this->dateFrom !== null;
    }

    /**
     * @param \DateTime $dateFrom
     */
    public function setDateFrom($dateFrom)
    {
        $this->dateFrom = $dateFrom;
    }
}
