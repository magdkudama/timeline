<?php

namespace TimelineAppBundle\Timeline;

class SplittingUnits
{
    const YEARS = 'Y';
    const MONTHS = 'M';
}
