<?php

namespace TimelineAppBundle\Timeline;

use Doctrine\Common\Persistence\ObjectManager;
use TimelineAppBundle\Entity\Repository\EventRepository;
use TimelineAppBundle\Entity\Timeline;
use TimelineAppBundle\Timeline\Filters\EventFilters;

class TimelineGenerator
{
    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @param ObjectManager $objectManager
     */
    public function __construct(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param EventFilters $filters
     * @return array
     */
    public function guessSplittingStrategy(EventFilters $filters)
    {
        $intervalDifference = $filters->getDateTo()->diff($filters->getDateFrom());

        $unit  = SplittingUnits::MONTHS;
        $value = 1;

        if ($intervalDifference->format('%Y') >= 40) {
            $unit  = SplittingUnits::YEARS;
            $value = 5;
        } elseif ($intervalDifference->format('%Y') > 1) {
            $unit  = SplittingUnits::YEARS;
            $value = 1;
        }

        return [
            'unit' => $unit,
            'value' => $value
        ];
    }

    /**
     * @param Timeline $timeline
     * @param EventFilters $filters
     * @return array
     */
    public function getBuckets(Timeline $timeline, EventFilters $filters)
    {
        $startEventDate = $filters->getDateFrom();
        $endEventDate = $filters->getDateTo();

        $splitting = $this->guessSplittingStrategy($filters);

        $bucketsInterval = new \DateInterval('P' . $splitting['value'] . $splitting['unit']);
        $start = clone $startEventDate;

        $buckets = [];

        while ($start->diff($endEventDate)->format('%R') === '+') {
            if (!empty($buckets)) {
                $start->modify('+1 second');
                $start->setTime(0, 0, 0);
            }

            $end = clone $start;
            $end->add($bucketsInterval);
            $end->setTime(23, 59, 59);

            $buckets[] = [$start, $end];
            $start = clone $end;
        }

        $buckets[count($buckets) - 1][1] = $endEventDate;

        $result = [];

        foreach ($buckets as $bucket) {
            /** @var $startDate \DateTime */
            $startDate = $bucket[0];
            /** @var $endDate \DateTime */
            $endDate = $bucket[1];

            $filters->setDateFrom($startDate);
            $filters->setDateTo($endDate);

            $result[] = [
                'start_time' => $startDate->getTimestamp(),
                'end_time' => $endDate->getTimestamp(),
                'total_events' => $this->getEventRepository()->countEvents($timeline, $filters),
                'events' => $this->getEventRepository()->findEvents($timeline, $filters, 2)
            ];
        }

        return $result;
    }

    /**
     * @return EventRepository
     */
    private function getEventRepository()
    {
        return $this->objectManager->getRepository('TimelineAppBundle:Event');
    }
}