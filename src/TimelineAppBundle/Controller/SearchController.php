<?php

namespace TimelineAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use TimelineAppBundle\Entity\Repository\TimelineRepository;

class SearchController extends Controller
{
    public function searchAction(Request $request)
    {
        $searchResults = $this->getTimelineRepository()->search(
            $request->query->get('term', '')
        );

        return new JsonResponse($searchResults);
    }

    /**
     * @return TimelineRepository
     */
    private function getTimelineRepository()
    {
        return $this->getDoctrine()->getRepository('TimelineAppBundle:Timeline');
    }
}
