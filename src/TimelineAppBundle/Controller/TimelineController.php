<?php

namespace TimelineAppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use TimelineAppBundle\Entity\Repository\EventRepository;
use TimelineAppBundle\Entity\Repository\TimelineRepository;
use TimelineAppBundle\Entity\Timeline;
use TimelineAppBundle\Timeline\Filters\EventFilters;
use TimelineAppBundle\Timeline\TimelineGenerator;

class TimelineController extends Controller
{
    public function indexAction($user, $timeline)
    {
        $timeline = $this
            ->getTimelineRepository()
            ->findBySlugs($user, $timeline);

        return $this->render('TimelineAppBundle:Timeline:index.html.twig', [
            'timeline' => $timeline
        ]);
    }

    public function eventsAction(Timeline $timeline)
    {
        $eventFilters = $this->getEventFilters();
        $eventFilters->create();

        if (!$eventFilters->hasDateFrom()) {
            $fromDate = $this->getEventRepository()->findFirstEvent($timeline)->getDate();
            $eventFilters->setDateFrom($fromDate);
        }

        if (!$eventFilters->hasDateTo()) {
            $toDate = $this->getEventRepository()->findLastEvent($timeline)->getDate();
            $eventFilters->setDateTo($toDate);
        }

        return new JsonResponse([
            'start_time' => $eventFilters->getDateFrom()->getTimestamp(),
            'end_time' => $eventFilters->getDateTo()->getTimestamp(),
            'breakpoint' => $this->getTimelineGenerator()->guessSplittingStrategy($eventFilters),
            'intervals' => $this->getTimelineGenerator()->getBuckets($timeline, $eventFilters)
        ]);
    }

    /**
     * @return EventFilters
     */
    private function getEventFilters()
    {
        return $this->get('timeline.events.filters');
    }

    /**
     * @return EventRepository
     */
    private function getEventRepository()
    {
        return $this->getDoctrine()->getRepository('TimelineAppBundle:Event');
    }

    /**
     * @return TimelineRepository
     */
    private function getTimelineRepository()
    {
        return $this->getDoctrine()->getRepository('TimelineAppBundle:Timeline');
    }

    /**
     * @return TimelineGenerator
     */
    private function getTimelineGenerator()
    {
        return $this->get('timeline.generator');
    }
}
