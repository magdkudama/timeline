<?php

namespace TimelineAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TimelineAppBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('Ganesh Kumar');
        $user1->setPassword('1');
        $user1->setEmail('ganesh@gmail.com');
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('Magd Kudama');
        $user2->setPassword('2');
        $user2->setEmail('magd@gmail.com');
        $manager->persist($user2);

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
