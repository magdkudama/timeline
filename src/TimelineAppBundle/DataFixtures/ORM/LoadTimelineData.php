<?php

namespace TimelineAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TimelineAppBundle\Entity\Timeline;

class LoadTimelineData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $timeline1 = new Timeline();
        $timeline1->setName('Steve Jobs');
        $timeline1->setDescription('Steve Jobs description');
        $timeline1->setImage('http://www.nowyouknowfacts.com/wp-content/uploads/2015/01/SteveJobsCloseUp.jpeg');
        $timeline1->setType('person');

        $user = $manager->getRepository('TimelineAppBundle:User')->findOneBy(['username' => 'Ganesh Kumar']);

        $timeline1->setUser($user);
        $manager->persist($timeline1);

        $manager->flush();
    }

    public function getOrder()
    {
        return 20;
    }
}
