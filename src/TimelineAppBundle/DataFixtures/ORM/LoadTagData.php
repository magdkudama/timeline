<?php

namespace TimelineAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TimelineAppBundle\Entity\Tag;

class LoadTagData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $tag1 = new Tag();
        $tag1->setText('life');
        $manager->persist($tag1);

        $tag2 = new Tag();
        $tag2->setText('other');
        $manager->persist($tag2);

        $tag3 = new Tag();
        $tag3->setText('new');
        $manager->persist($tag3);

        $manager->flush();
    }

    public function getOrder()
    {
        return 10;
    }
}
