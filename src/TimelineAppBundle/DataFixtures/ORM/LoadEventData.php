<?php

namespace TimelineAppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use TimelineAppBundle\Entity\Event;

class LoadEventData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $timeline = $manager->getRepository('TimelineAppBundle:Timeline')->findOneBy(['name' => 'Steve Jobs']);
        $tag1 = $manager->getRepository('TimelineAppBundle:Tag')->findOneBy(['text' => 'life']);
        $tag2 = $manager->getRepository('TimelineAppBundle:Tag')->findOneBy(['text' => 'other']);

        $event1 = new Event();
        $event1->setTitle('Steve was born');
        $event1->setDescription('Steve started apple, wow!');
        $event1->setType('link');
        $event1->setMilestone(true);
        $date1 = \DateTime::createFromFormat('Y-m-d', '1950-10-03');
        $event1->setDate($date1);
        $event1->setUrl('http://bbc.co.uk/steve-jobs');
        $event1->setTimeline($timeline);
        $event1->addTag($tag1);
        $event1->addTag($tag2);
        $manager->persist($event1);

        $event2 = new Event();
        $event2->setTitle('Steve started Apple');
        $event2->setDescription('Steve at apple, wow!');
        $event2->setType('image');
        $event2->setMilestone(false);
        $date2 = \DateTime::createFromFormat('Y-m-d', '1961-01-12');
        $event2->setDate($date2);
        $event2->setUrl('http://bbc.co.uk/steve-jobs.jpg');
        $event2->setTimeline($timeline);
        $event2->addTag($tag1);
        $manager->persist($event2);

        $event3 = new Event();
        $event3->setTitle('Steve creates Macintosh');
        $event3->setDescription('Steve started apple, wow!');
        $event3->setType('link');
        $event3->setMilestone(false);
        $date3 = \DateTime::createFromFormat('Y-m-d', '1965-01-10');
        $event3->setDate($date3);
        $event3->setUrl('http://bbc.co.uk/steve-jobs');
        $event3->setTimeline($timeline);
        $event3->addTag($tag2);
        $manager->persist($event3);

        $event4 = new Event();
        $event4->setTitle('Jobs gets fired from Apple');
        $event4->setDescription('Steve started apple, wow!');
        $event4->setType('link');
        $event4->setMilestone(true);
        $date4 = \DateTime::createFromFormat('Y-m-d', '1966-01-10');
        $event4->setDate($date4);
        $event4->setUrl('http://bbc.co.uk/steve-jobs');
        $event4->setTimeline($timeline);
        $manager->persist($event4);

        $event5 = new Event();
        $event5->setTitle('Jobs comes back and creates iPad');
        $event5->setDescription('Steve started apple, wow!');
        $event5->setType('link');
        $event5->setMilestone(true);
        $date5 = \DateTime::createFromFormat('Y-m-d', '1970-10-10');
        $event5->setDate($date5);
        $event5->setUrl('http://bbc.co.uk/steve-jobs');
        $event5->setTimeline($timeline);
        $manager->persist($event5);

        $event6 = new Event();
        $event6->setTitle('Steve jobs dies...');
        $event6->setDescription('Steve started apple, wow!');
        $event6->setType('link');
        $event6->setMilestone(true);
        $date6 = \DateTime::createFromFormat('Y-m-d', '1990-10-30');
        $event6->setDate($date6);
        $event6->setUrl('http://bbc.co.uk/steve-jobs');
        $event6->setTimeline($timeline);
        $manager->persist($event6);

        $manager->flush();
    }

    public function getOrder()
    {
        return 40;
    }
}
